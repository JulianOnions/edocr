# README #

Basic stuff needed for OCR image recognition of trading prices

You need perl installed to run everything, but that might change. Maybe python.

It currently only runs on my resolution screenshots, that is something to fix. Maybe % of the image.

To run it run

```
cmd.bat Image.bmp
```

This will snip bits out of the image and run the OCR package on them.
You can then run


```
perl assemble.pl
```


to attempt to bring it all together. 

I want eventually to do cross checks between the whole panel and the columns, as sometimes the OCR gets things way out of order. 
Also adding in range checks so that, say, waste has to be between 0 and 100, and a value of 6 million 
is not allowed.