#!/usr/bin/perl

use strict;
use warnings;

sub fixheader {
    my $header = "";
    open (FILE, "header.txt") || die "Can't open header";
    while (<FILE>) {
	next if (/^[I_ ]*$/);
#	print "HEADER $_";
	s/_/ /g;
	s/ I //g;
	$header .= $_;
    }
    return $header;
}

sub findgoods {
    my @goods;
    open (FILE, "goods.txt") || die "Can't open goods";
    while (<FILE>) {
	next if (/^[I_ ]*$/);
	chomp;
	s/DO M./DOM./;
	s/APPLIA\s*N\s*CES/APPLIANCES/;
	s/FIS H/FISH/;
	s/G R AIN/GRAIN/;
	s/CONSUMERTECHNOLOGY/CONSUMER TECHNOLOGY/;
	push(@goods, $_);
    }
    return @goods;
}

sub findvalues {
    my $sell = [];
    my $buy = [];
    open (FILE, "value.txt") || die "Can't open values";
    while (my $line = <FILE>) {
	next if ($line =~ /^[I_ ]*$/);
	chomp($line);
	$line =~ s/(\d) (\d)/$1$2/g;
	$line =~ s/([\d,]) ([\d,])/$1$2/g;
	$line =~ s/(\d),(\d)/$1$2/g;
	$line =~ s/(\d)_(\d)/$1X$2/g;
	$line =~ s/_+//g;
	my($b, $s) = split(/\s\s+/, $line, 2);
#	print "VALUE $line -> $b $s\n";
	push(@{$buy}, $b);
	push(@$sell, $s);
    }
    return $buy, $sell;
}
sub finddemand {
    my $file = shift;
    my $dem = [];
    my $hml = [];
    open (FILE, $file) || die "Can't open $file";
    while (my $line = <FILE>) {
	next if ($line =~ /^[I_ ]*$/);
	chomp($line);
	$line =~ s/(\d) (\d)/$1$2/g;
	$line =~ s/([\d,]) ([\d,])/$1$2/g;
	$line =~ s/(\d),(\d)/$1$2/g;
	$line =~ s/(\d)_(\d)/$1X$2/g;
	$line =~ s/_+//g;
	my($b, $s) = split(/\s\s+/, $line, 2);
#	print "VALUE $line -> $b $s\n";
	$s =~ s/I\s+(MED|HIGH|LOW)/$1/ if ($s);
	push(@$dem, ($b || "-"));
	push(@$hml, ($s || "-"));
    }
    return $dem, $hml;
}

sub findGA {
    my $file = shift;
    my @GA;
    open (FILE, $file) || die "Can't open $file";
    while (my $line = <FILE>) {
	next if ($line =~ /^[I,0_ ]*$/);
	chomp($line);
	$line =~ s/(\d) (\d)/$1$2/g;
	$line =~ s/([\d,]) ([\d,])/$1$2/g;
	$line =~ s/(\d),(\d)/$1$2/g;
	$line =~ s/(\d)_(\d)/$1X$2/g;
	$line =~ s/_+//g;
#	print "VALUE $line -> $b $s\n";
	$line =~ s/C_/CR/;
	$line =~ s/_R/CR/;
	push(@GA, ($line || "-"));
    }
    return @GA
}



my $page = fixheader();
my @goods = findgoods();
my($buy, $sell) = findvalues();
my($dem, $dhml) = finddemand("demand.txt");
my($sup, $shml) = finddemand("supply.txt");
my @GA = findGA("GA.txt");
warn "Not matching " if (@goods != @$buy);

print $page;
for (my $i = 0; $i < @goods; $i++) {
    printf "%-20s %-7s %3s %7s %3s  %7s %3s %s\n", $goods[$i], 
    $buy->[$i],  $sell->[$i], 
    $dem->[$i], $dhml->[$i],
    $sup->[$i] || "-", $shml->[$i] || "-",
    $GA[$i];
}
